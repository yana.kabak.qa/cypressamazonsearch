cy.utils = {
    insert(str, symbol, index)
    {
        return str.substring(0, index) + symbol + str.substring(index);
    },

    replaceCh(str) {
        let st = str.split('|')[0];
        st = st.split('by')[1]
        st = st.replace(/ +/g, " ");
        st = st.slice(1, -1)
        return st;
    },

    replaceChAfter(str){
        return str.split('-')[0]
    }

}