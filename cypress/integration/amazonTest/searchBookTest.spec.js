import MainPage from "../pageObject/findBook";
import FindingBooksPage from "../pageObject/findingBooksPage";

describe('Amazon Mane Page',() => {
    const mainPage = new MainPage()
    const findingBooksPage = new FindingBooksPage()

    it('Visiting Amazon to search book', () => {
        cy.visit(cy.constants.mainPage)
        cy.wait(10000)
        mainPage.search()
        mainPage.clickSearchButton()
        findingBooksPage.assertBooks()
    })
})

