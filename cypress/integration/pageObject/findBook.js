class MainPage {

    search(){
        return cy.get(cy.constants.searchArea)
            .type(cy.constants.keyWord)
            .click()
            .get(cy.constants.searchCategoriesButton)
            .select(cy.constants.selectCategories, {force: true})
    }

    clickSearchButton(){
        return cy.get(cy.constants.searchButton).click()
    }
}

export default MainPage