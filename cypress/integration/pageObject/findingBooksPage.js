class FindingBooksPage{
    assertBooks(){
        cy.get(cy.constants.nameBookList).then(fun => {
            let length = fun.length
            cy.functions.checkBestSeller(length)
            cy.functions.checkBookList(length, fun)
        })
    }
}

export default FindingBooksPage